var Calc = function () {

    var config = {
            valueAttr   : "data-value",
            emptyStr    : "",
            valueRegexp : /\d*\.*\d*/ig,
            spaces      : /\s{2,}/g,
            multiDots   : /\.{2,}/g,
            dot         : ".",
            ltrim       : /^\s+/g,
            zero        : 0
        },
        /**
         * Selectors object
         * @type {{btn: string, input: string, buttonsPlaceholder: string, inputedValues: string}}
         */
        selectors = {
            btn                : ".btn",
            input              : ".main-input",
            buttonsPlaceholder : ".calc-buttons",
            inputedValues      : ".inputed-values"
        },
        /**
         * Matematic operations
         * @type {{/: divide, *: multiply, -: subtraction, +: addition}}
         */
        mathActionsMapper = {
            "/" : divide,
            "*" : multiply,
            "-" : subtraction,
            "+" : addition
        },
        /**
         * Calculator actions
         * @type {{=: equal, c: clear, .: addDot}}
         */
        calcActions = {
            "=" : equal,
            "c" : clear,
            "." : addDot
        },
        /**
         * Char codes for operations
         * @type {{13: (function(this:Calc)), 46: (function(this:Calc)), 111: (function(this:Calc)), 106: (function(this:Calc)), 107: (function(this:Calc)), 109: (function(this:Calc)), 110: (function(this:Calc)), 190: (function(this:Calc))}}
         */
        charCodes = {
            "13"  : equal.bind(this),
            "46"  : clear.bind(this),
            "111" : setCommand.bind(this, "/"),
            "106" : setCommand.bind(this, "*"),
            "107" : setCommand.bind(this, "+"),
            "109" : setCommand.bind(this, "-"),
            "110" : addDot.bind(this),
            "190" : addDot.bind(this)
        };
    /**
     * DOM elements will be here.
     * @type {{btn: string, input: string, buttonsPlaceholder: string, inputedValues: string}}
     */
    this.elements = {};
    /**
     * Method to initialize Calc.prototype.elements object
     * @returns {Calc}
     */
    this.initElements = function () {
        for (var selector in selectors) {
            this.elements[selector] = document.querySelectorAll(selectors[selector]);
        }
        return this;
    };
    /**
     * Will attach event listeners to DOM elements
     */
    this.addHandlers = function () {
        this.elements.buttonsPlaceholder[0].addEventListener("click", onClickBtn.bind(this));
        this.elements.input[0].addEventListener("keyup", onKeyup.bind(this));
    };

    this.setCalcDefaults = function () {
        this.value = undefined;
        this.elements.inputedValues[0].innerHTML = config.emptyStr;
        this.command = config.emptyStr;
        this.elements.input[0].value = config.zero;
    };

    /**
     * keyUp event handler
     * @param e {Event}
     */
    function onKeyup (e) {
        var keyCode = e.keyCode;
        validateText(e);
        if (this.clearInput && !charCodes[keyCode]) {
            this.elements.input[0].value = String.fromCharCode(keyCode);
            this.clearInput = false;
        }
        charCodes[keyCode] && charCodes[keyCode].call();
    }

    /**
     * click event handler
     * @param e {Event}
     */
    function onClickBtn (e) {
        e.preventDefault();
        var target = e.target;
        var value = target.getAttribute(config.valueAttr) !== null && target.getAttribute(config.valueAttr).toLowerCase();

        if (!parseFloat(value) && value !== "0") {
            setCommand.call(this, value);
        } else {
            if (this.clearInput) {
                this.elements.input[0].value = config.emptyStr;
                this.clearInput = false;
            }
            this.elements.input[0].value = value ? this.elements.input[0].value + value : this.elements.input[0].value;
        }
    }

    /**
     * Will set Calc.prototype.value, call calcAction method if needed.
     * Set Calc.prototype.command and Calc.prototype.clearInput flag
     * @param command
     * @returns {boolean}
     */
    function setCommand (command) {
        if (calcActions[command]) {
            calcActions[command].call(this);
            return false;
        }
        this.command && equal.call(this);
        setValue.call(this);
        this.elements.inputedValues[0].innerHTML = this.value + " " + command;
        this.clearInput = true;
        this.command = command;
    }

    /**
     * will set Calc.prototype.value from input
     */
    function setValue () {
        this.value = parseFloat(this.elements.input[0].value);
    }

    function divide () {
        return this.value / parseFloat(this.elements.input[0].value);
    }

    function multiply () {
        return this.value * parseFloat(this.elements.input[0].value);
    }

    function subtraction () {
        return this.value - parseFloat(this.elements.input[0].value);
    }

    function addition () {
        return this.value + parseFloat(this.elements.input[0].value);
    }

    /**
     * Will add dot to value if needed.
     */
    function addDot () {
        this.elements.input[0].value = this.clearInput ? config.zero + config.dot : this.elements.input[0].value;
        this.elements.input[0].value = this.elements.input[0].value.indexOf(".") === -1 ? this.elements.input[0].value + config.dot : this.elements.input[0].value;
        this.elements.input[0].value = this.elements.input[0].value.match(config.valueRegexp)[0];
        this.clearInput = false;
    }

    /**
     * Will do mathematics opperations
     */
    function equal () {
        var newValue = mathActionsMapper[this.command] && mathActionsMapper[this.command].call(this);
        this.elements.input[0].value = newValue ? newValue : this.elements.input[0].value;
        this.value = newValue ? newValue : this.value;
        this.elements.inputedValues[0].innerHTML = config.emptyStr;
        this.command = config.emptyStr;
    }

    /**
     * Clear calculator inputs
     */
    function clear () {
        this.setCalcDefaults();
    }

    /**
     * Will validate input value and remove not allowed symbols
     * @param e {Event}
     */
    function validateText (e) {
        var target = e.currentTarget,
            selectionStart = target.selectionStart,
            selectionEnd = target.selectionEnd,
            text = target.value,
            pattern = config.valueRegexp,
            matched = text.match(pattern),
            trimedLeft = [],
            removeMultiDot = [],
            removeMultiSpace = [];

        if (matched) {
            matched.forEach(function (el, index) {
                // remove spaces or dots from the beginning
                trimedLeft[index] = matched[index].replace(config.ltrim, config.emptyStr);
                // remove dot when it's met more than one in a row
                removeMultiDot[index] = trimedLeft[index].replace(config.multiDots, config.dot);
                removeMultiSpace[index] = removeMultiDot[index].replace(config.spaces, config.emptyStr);
            });
        }

        var value = removeMultiSpace.join(config.emptyStr),
            difference = text.length - value.length;
        //if difference is greater than 0 it means has been removed forbidden symbols and cursor will be set on properly position
        target.value = value;
        // set cursor previous position
        target.selectionStart = selectionStart - difference;
        target.selectionEnd = selectionEnd - difference;
    }

    this.initElements().addHandlers();
    this.setCalcDefaults();
};

var calc = new Calc();
